'use strict';

// anchor policy links
$('.anchor').click(function () {
	$('.anchor').removeClass('active');
	$(this).addClass('active');
	var scroll_el = $(this).attr('href');
	if ($(scroll_el).length != 0) {
		$('html, body').animate({scrollTop: $(scroll_el).offset().top - 0}, 1000);
	}
	return false;
});

$(document).ready(function(){

	// Checking for email
	$('.js-val-email').each(function () {
		$(this).on('keyup', function () {
			if($(this).val().match('^[a-zA-Z0-9_.-@]*$')) {

			} else {
				// $(this).val('');
				$(this).val($(this).val().slice(0, ($(this).val().length - 1)));
			}
		});
	});

	// Time Range Progress Bar
	// var data_year = $('.bar-line').data('year'),
	// 	data_month = $('.bar-line').data('month'),
	// 	data_day = $('.bar-line').data('day'),
	// 	data_year_to = $('.bar-line').data('year_to'),
	// 	data_month_to = $('.bar-line').data('month_to'),
	// 	data_day_to = $('.bar-line').data('day_to');
/*

	var dataTime = {
		year:  $('.bar-line').data('year'),
		month: $('.bar-line').data('month'),
		day: $('.bar-line').data('day'),
		to_year:  $('.bar-line').data('year_to'),
		to_month: $('.bar-line').data('month_to'),
		to_day: $('.bar-line').data('day_to')
	};

	function timerLeft(dataTime) {
		var last;
		var setIntervalID = 0;

console.log( dataTime );

		setIntervalID = setInterval(function() {
			var hoursLeft = $('.hours'),
				minutesLeft = $('.minutes'),
				secLeft = $('.seconds'),

				dateNow = new Date().getTime(),

				dateStart = new Date(dataTime.year, dataTime.month, dataTime.day).getTime(),
				dateFinish = new Date(dataTime.to_year, dataTime.to_month, dataTime.to_day).getTime(),
				//sec = Math.floor((dateFinish - dateNow) / 1000),
				sec = Math.floor((dateFinish - dateNow) / 1000),
				secToMinutes = sec % 3600,

				hoursNumber = Math.floor(sec / 3600),
				minutesNumber = Math.floor(secToMinutes / 60),
				secondsNumber = secToMinutes % 60;

			hoursLeft.text(hoursNumber);
			minutesLeft.text(minutesNumber);
			secLeft.text(secondsNumber);

			last = parseInt(sec);

			console.log( new Date() );

			// Animate waves
			$('.bar-line').css({
				'animation-duration': last + 's'
			});

			if( last <= 0 ){
				hoursLeft.text(0);
				minutesLeft.text(0);
				secLeft.text(0);
				$('.bar-line .circle').css('display', 'flex');
				$('.bar-line .inline').css('display', 'none');
				$('.circle__seconds').css('display', 'none');
				$('.bar-line').css({
					'animation-duration': 0 + 's',
					'width': '45px'
				});
				clearInterval(setIntervalID);
			}
		}, 1000);
	}
	timerLeft(dataTime);
*/

	var data_year = $('.bar-line').data('year'),
		data_minutes = $('.bar-line').data('minutes'),
		data_day = $('.bar-line').data('day');

	function timerLeft(year, month, day) {
		var last;
		var setIntervalID = 0;

		setIntervalID = setInterval(function() {
			var hoursLeft = $('.hours'),
				minutesLeft = $('.minutes'),
				secLeft = $('.seconds'),

				dateNow = new Date().getTime(),
				dateFinish = new Date(year, month-1, day).getTime(),
				sec = Math.floor((dateFinish - dateNow) / 1000),
				secToMinutes = sec % 3600,

				hoursNumber = Math.floor(sec / 3600),
				minutesNumber = Math.floor(secToMinutes / 60),
				secondsNumber = secToMinutes % 60;

			hoursLeft.text(hoursNumber);
			minutesLeft.text(minutesNumber);
			secLeft.text(secondsNumber);

			last = parseInt(sec);

			// Animate waves
			$('.bar-line').css({
				'animation-duration': last + 's'
			});

			if( last <= 0 ){
				hoursLeft.text(0);
				minutesLeft.text(0);
				secLeft.text(0);
				$('.bar-line .circle').css('display', 'flex');
				$('.bar-line .inline').css('display', 'none');
				$('.bar-line .circle__seconds').css('display', 'none');
				$('.bar-line').css({
					'animation-duration': 0 + 's',
					'width': '45px'
				});
				clearInterval(setIntervalID);
			}
		}, 1000);
	}
	timerLeft(data_year, data_minutes, data_day);


	// $('.js-modal_friends').addClass('active');

	var productSlider = $('.product .product__images'),
		notAvailable = $('.not-available .product__images'),
		discountProductSlider = $('.discount-product__images'),
		contactSlider = $('.reviews-slider'),
		likesCounter = 0;

	$('.reviews__slider').slick({
		dots: false,
		infinite: true,
		speed: 1500,
		arrows: true,
		slidesToShow: 4,
		responsive: [
			{
				breakpoint: 1340,
				settings: {
					slidesToShow: 2,
					variableWidth: true
				}
			},
			{
				breakpoint: 576,
				settings: {
					dots: true,
					slidesToShow: 1
				}
			},
		]
	});

	// productSlider.not('.slick-initialized').slick({
	// 	dots: true,
	// 	infinite: true,
	// 	autoplay: false,
	// 	speed: 300,
	// 	slidesToShow: 1,
	// 	arrows: false
	// });

	if( $(window).width() < 992 ){
		//$('.cart-sidebar-view-product .product .product__images').not('.slick-initialized').slick('unslick');

		$('.js-slider-tablet-items').not('.slick-initialized').slick({
			slidesToShow: 2,
			slidesToScroll: 1,
			arrows: true,
			dots: true,
			infinite:true,
			focusOnSelect: false,
			variableWidth: true,
			adaptiveHeight: true,
			swipe:true,
			prevArrow: '<div class="prev-not-found"></div>',
			nextArrow: '<div class="next-not-found"></div>',
			responsive: [
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
				}
			]
		});

	}

	// Unslick if not available
	//notAvailable.slick('unslick');

	// discountProductSlider.slick({
	// 	dots: true,
	// 	infinite: true,
	// 	speed: 1500,
	// 	slidesToShow: 1,
	// 	arrows: false
	// });

	$('.reviews-slider').slick({
		dots: false,
		infinite: true,
		speed: 1500,
		arrows: true,
		slidesToShow: 4,
		accessibility: false,
		responsive: [
			{
				breakpoint: 1280,
				settings: {
					slidesToShow: 3,
					prevArrow: "<div class='slick-prev'></div>",
					nextArrow: "<div class='slick-next'></div>"
				}
			},
			{
				breakpoint: 1170,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 766,
				settings: {
					slidesToShow: 1
				}
			},
			{
				breakpoint: 481,
				settings: {
					slidesToShow: 1,
					dots:true
				}
			},
		]
	});

	// News filter
	$('.articles__nav-item').click(function (e) {
		e.preventDefault();
		$(this).addClass('active');
		$(this).siblings().removeClass('active')
	});

	$('.hover-slider').hover(
		function() {
			$(this).addClass('hover-show');
		},
		function() {
			$(this).removeClass('hover-show');
		}
	);

	// Invite Friends
	$('.product__invite-btn, .product__invite-title').click(function () {
		$(this).parent().find('.product__invite-list').toggleClass('active');
		$(this).closest('.product').toggleClass('active');
	});

	$('.js-invite-friends').click(function () {
		$(this).parent().find('.product__invite-list').toggleClass('active');
		$(this).closest('.js-invite-parent').toggleClass('active');
	});

	$('.product__like, .discount-sidebar .discount-product__like').click(function () {
		if ($(this).hasClass('liked')) {
			$(this).removeClass('liked').find('.product__like-count').text(--likesCounter);
		}
		else {
			$(this).addClass('liked').find('.product__like-count').text(++likesCounter);
		}
	});

	$('.product-slider-slides').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		swipe: false,
		asNavFor: '.product-slider-nav'
	});
	$('.product-slider-nav').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.product-slider-slides',
		vertical: true,
		focusOnSelect: true,
		arrows: true,
		dots: false,
		prevArrow: '<svg class="slick-prev" width="11" height="7" viewBox="0 0 11 7" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="7.56931" height="1.08133" rx="0.540665" transform="translate(0.779297 0.281738) scale(1.01876 0.980885) rotate(45)" fill="#C4C4C4"/><rect width="7.56931" height="1.08133" rx="0.540665" transform="translate(4.76758 5.53174) scale(1.01876 0.980885) rotate(-45)" fill="#C4C4C4"/></svg>',
		nextArrow: '<svg class="slick-next" width="11" height="7" viewBox="0 0 11 7" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="7.56931" height="1.08133" rx="0.540665" transform="translate(0.779297 0.281738) scale(1.01876 0.980885) rotate(45)" fill="#C4C4C4"/><rect width="7.56931" height="1.08133" rx="0.540665" transform="translate(4.76758 5.53174) scale(1.01876 0.980885) rotate(-45)" fill="#C4C4C4"/></svg>',
		responsive: [
			{
				breakpoint: 768,
				settings: {
					vertical: false
				}
			},
		]
	});

	$('.favorite svg').click(function () {
		$(this).toggleClass('liked');
	});

// CART
	$(function(){
		for( var i = 0; i < $('.js-count-plus').length; i++ ){
			get_summPoduct( $('.js-count-plus').eq(i) );
		}
		get_sumOrder();
		get_keydownOrder();
		// Sum Product in CART
		function get_summPoduct(el){
			var input_value = el.closest('.item-amount').find('.js-val-input').val();
			var price_item = el.closest('.non-block').find('.js-cart-price').text();
			var sum_item_input = el.closest('.non-block').find('.js-input-cart-price');
			var sum_item = parseInt(input_value) * parseInt(price_item);

			if( sum_item != NaN && sum_item != undefined ){
				sum_item_input.val( sum_item );
			}
		}

		function get_sumOrder(){
			var order_sum = 0;
			setTimeout(function(){
				for( var i = 0; i < $('.js-input-cart-price').length; i++ ){
					var sum_item = parseInt( $('.js-input-cart-price').eq(i).val() );
					order_sum += sum_item;
				}
				if( $('.js-cart-sum').length > 0 ) {
					if (order_sum != NaN && order_sum != undefined) {
						$('.js-cart-sum').text(order_sum);
					}
					else {
						$('.js-cart-sum').text(0);
					}
				}
			},20);
		}

		function get_keydownOrder(){
			var input_amount = $('.js-val-input');
			var pattern = '^[1-9]*[.,]?[0-9]+$';

			input_amount.each(function(){
				$(this).on('keyup', function(){
					// console.log( $(this).val() );

					if( $(this).val().match(pattern) ){
						$(this).val( parseInt( $(this).val() ) );
					}else if ($(this).val() == '') {
						$(this).val(0);
					}
					else{
						$(this).val($(this).val().slice(0, ($(this).val().length - 1)));
					}

					get_summPoduct( $(this).next('.js-count-plus') );
					get_sumOrder();
				})
			});
		}

		// Counter boxes
		$('.js-count-plus').each(function () {

			$(this).on('click', function () {
				var value = $(this).closest('.item-amount').find('.js-val-input').val();
				var number = parseInt(value);
				number++;
				$(this).closest('.item-amount').find('.js-val-input').val(number);

				get_summPoduct($(this));
				get_sumOrder();
			});
		});

		$('.js-count-minus').each(function () {
			$(this).on('click', function (event) {
				event.preventDefault();
				var value = $(this).closest('.item-amount').find('.js-val-input').val();
				var number = parseInt(value);
				number--;
				if (number <= 0) {
					number = 0;
				}
				$(this).closest('.item-amount').find('.js-val-input').val(number);

				get_summPoduct($(this));
				get_sumOrder();
			});
		});
		// End Counter boxes

		// remove product from cart
		$('.js-cart-item_remove').on('click', function () {
			$(this).closest('.js-cart-item').remove();
			get_sumOrder();
		});
		/*		$('.js-cart-item-close-mobile').on('click', function () {
		 $(this).parent().parent().parent().remove();
		 });*/
	});
	// End CART

	// Catalog Range Slider
	$("#catalog-range-slider").editRangeSlider({
		step: 1,
		bounds: {
			min: 0,
			max: 200
		}
	});

	$("#catalog-range-slider_price").editRangeSlider({
		step: 100,
		bounds: {
			min: 0,
			max: 40000
		}
	});
	// End of Catalog Range Slider

	// Show Filter Modal
	$('.js-show-filter').on('click', function () {
		var self = this;
		showFilterModal(self);
	});

	$('.ui-editRangeSlider-inputValue').on('change', function () {
		showFilterModal($(this));
	});

	$('.ui-rangeSlider-handle').on('click', function () {
		showFilterModal($(this));
	}).on('touchstart', function () {
		showFilterModal($(this));
	});

	$('.js-show-modal-find').click(function (e) {
		e.preventDefault();
		$('.js-show-modal').removeClass('show-newproduct-modal');
	});
	// End Show Filter Modal

	$(".custom-checkboxes .js-show-filter").each(function () {
		console.log($(this).offset().top);
		var modal = $(this).closest('.js-show-parent').find('.js-show-modal');
		$(this).click(function () {
			var showFilterOffset = $(this).offset().top,
					parrentOffset = $(this).closest('.js-show-parent').offset().top,
					modalOffset = showFilterOffset - parrentOffset;

			$('.js-show-modal').removeClass('show-newproduct-modal');
			modal.removeClass('show-newproduct-modal', function () {
				setTimeout(function () {
					modal.addClass('show-newproduct-modal').css("top", modalOffset);
				}, 500);
			});
		});
	});


	// Screen Size Ашдеук
	$('.js-screen-size').click(function (e) {
		e.preventDefault();
		$(this).toggleClass('active');
	});
	// End of Screen Size Modal
	// Payment
	$('.js-payment').click(function (e) {
		e.preventDefault();
		$(this).toggleClass('active').siblings().removeClass('active');
	});
	// End Payment

	// Delivery
	// var deliveryRadio = null;
	$('.js-delivery').click(function () {
		$('.js-delivery').removeClass('is-active');
		$(this).addClass('is-active');
		// deliveryRadio = $('input:radio[name="delivery"]:checked').val();
		// console.log(deliveryRadio);
	});
	// End Delivery

	// Payment
	$('.js-payment').click(function () {
		$('.js-payment').removeClass('is-active');
		$(this).addClass('is-active');
	});
	// End Payment

	// Filter Friends
	$('.js-filter-open').click(function () {
		$('.my-friends__filter-mobile-wrapper').addClass('show');
	});

	$('.js-filter-close').click(function () {
		$('.my-friends__filter-mobile-wrapper').removeClass('show');
	});
	// End Filter Friends

	// Catalog Filter
	$('.js-catalog-filter').click(function () {
		$('.catalog-filter, .filter-overlay').addClass('show').removeClass('closed');
		$('body').addClass('overflow');

		$(document).click(function (e) {
			if ($(event.target).closest(".js-catalog-filter").length || $(event.target).closest(".catalog-filter__container").length ) return;
			$('.catalog-filter, .filter-overlay').removeClass('show').addClass("closed");
			$('body').removeClass('overflow');
		});
	});

	$('.js-catalog-filter__close').click(function () {
		$('.catalog-filter, .filter-overlay').removeClass('show').addClass("closed");
		$('body').removeClass('overflow');
	});

	$('.header__menu-link_big-dropdown').click(function () {
		$('.big-dropdown').toggleClass('show');
		$(this).toggleClass('show');
	});

	$('.header__burger-mobile').click(function () {
		$(this).toggleClass('active');
		$('.mobile-dropdown').toggleClass('show');
	});

	// Hashtags
	$('.hashtags__item').click(function () {
		$(this).addClass('active').siblings().removeClass('active');
	});

	// Reset
	$('.js-newproduct-reset').on('click', function (e) {
		e.preventDefault();
		filterReset();
	});
	// End Reset

	// Filter Catalog Choose Several Colors
	$('.js-filter-color').on('click', function () {
		$(this).toggleClass('active');
	});

	// Cart Choose only one color
	$('.js-cart-color').each(function () {
		$('.js-cart-color-item').on('click', function () {
			$(this).addClass('active').siblings().removeClass('active');
		});
	});

	// Cart Choose only one size
	$('.js-cart-size').each(function () {
		$('.js-cart-size-item').on('click', function () {
			$(this).addClass('active').siblings().removeClass('active');
		});
	});

	sliderTab();

	// Accordeon Reference-information page
	$('.accordion_item').on('click', function () {
		$(this).toggleClass("active_block").siblings().removeClass("active_block");
		$(this).next(".info").slideToggle();
	});

});

// fansy box product card
// $(function($){
//     $().fancybox({
//         selector : '[data-fancybox="gallery"]',
//         loop     : true,
//         toolbar: "auto",
//         thumbs: {
//             autoStart: true,
//             hideOnClose: true,
//             parentEl: ".fancybox-container",
//             axis: "y"
//         },
//         buttons: [
//             "close"
//         ],
//         touch: {
//             vertical: true,
//             momentum: true
//         }
//     });
// });



// close:
//     '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}">' +
//     '<svg viewBox="0 0 40 40">' +
//     '<path d="M10,10 L30,30 M30,10 L10,30" />' +
//     "</svg>" +
//     "</button>",

// Onload document
$(window).on('load', function () {
	// Animate Order BarLine
	$('.js-order').addClass('active');

	$('.greeting__discount').addClass('show');


//	TIP cat
	$(function(){

		setTimeout(function(){
			$('.js-tip-cat').addClass('is-active');
		}, 100);
		// if( $(window).width() > 991 && $('.js-tip-cat').length > 0 ){
		// 	var _tipOffset = $('.tip-cat__wrapp').offset().top;
		//
		// 	 $(window).on('scroll',function(){
		// 		var _tip = $('.tip-cat__wrapp');
		// 		var _pageScroll = $(window).scrollTop();
		//
		// 		if( _pageScroll >= _pageScroll - _tip.height() ){
		// 			_tip.addClass('tip-cat__fixed');
		// 		}
		// 		if( _pageScroll <= _tipOffset ){
		// 			_tip.removeClass('tip-cat__fixed');
		// 		}
		// 	});
		// }

	});

	$(window).on('scroll', function () {
		// Fixed Provacy-Policy
		$(window).scrollTop() > 300 ? $('.policy-sidebar').addClass('fixed') : $('.policy-sidebar').removeClass('fixed');
	});
});



if($(window).width() <= 1700) {
	// $('.contacts__reviewsfb').slick({
	// 	slidesToShow: 1,
	// 	slidesToScroll: 1,
	// 	arrows: true,
	// 	dots: false,
	// 	infinite:true,
	// 	speed: 1500,
	// 	focusOnSelect: true,
	// 	prevArrow: '<div class="prev-not-found"></div>',
	// 	nextArrow: '<div class="next-not-found"></div>',
	// 	responsive: [
	// 		{
	// 			breakpoint: 481,
	// 			settings: {
	// 				dots:true
	// 			}
	// 		},
	// 	]
	// });
}

if($(window).width() <= 575) {
	$('.not-found-catalog .products-list').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		dots: true,
		infinite:true,
		focusOnSelect: false,
		variableWidth: true,
		adaptiveHeight: true,
		swipe:false,
		// centerMode: true,
		prevArrow: '<div class="prev-not-found"></div>',
		nextArrow: '<div class="next-not-found"></div>'
	});

  personalAreaSlider();

}

$('.slider-tablet-items').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    infinite:true,
    focusOnSelect: false,
    variableWidth: true,
    adaptiveHeight: true,
    swipe:true,
    prevArrow: '<div class="prev-not-found"></div>',
    nextArrow: '<div class="next-not-found"></div>',
    responsive: [
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 2,
            }
        },
        {
            breakpoint: 576,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});


if($(window).width() <= 650) {
	var size = 180,
		newsContent= $('.articles__item-info p'),
		newsText = newsContent.text();

	if(newsText.length > size){
		newsContent.text(newsText.slice(0, size) + ' ...');
	}
}
if($(window).width() <= 375) {
	var size = 90,
		newsContent= $('.articles__item-info p'),
		newsText = newsContent.text();

	if(newsText.length > size){
		newsContent.text(newsText.slice(0, size) + ' ...');
	}
}

$('.dataTable tbody tr').click(function () {

	if ($('.dataTable tbody tr').hasClass('selected')) {

		$('.tableGroupAction').addClass('one two');
	}
	else
	{
		$('.tableGroupAction').removeClass('one two');
	}
});

if($(window).width() > 1170) {
	$('.main-catalog__products-slider').removeClass('slick-slider-tab');
}

// Window on resize
$(window).on("resize", function () {
	sliderTab();

  if($(window).width() <= 575) {
    personalAreaSlider();
  }
});

// Slider for Products-list and for Sidebar Products Cards
function sliderTab() {
	if($(window).width() < 1700) {

		$('.slick-slider-tab').slick({
			dots: false,
			infinite: true,
			arrows: true,
			slidesToShow: 2,
			slidesToScroll: 1,
			prevArrow: "<div class='slick-prev'></div>",
			nextArrow: "<div class='slick-next'></div>",
			variableWidth: true,
			responsive: [
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 2,
						dots: true,
                        swipe: false
					}
				}
			]
		});

    $('.contacts__reviewsfb').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        infinite:true,
        focusOnSelect: false,
        variableWidth: true,
        adaptiveHeight: true,
        swipe:true,
        prevArrow: '<div class="prev-not-found"></div>',
        nextArrow: '<div class="next-not-found"></div>',
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

	}
}

// Catalog Filter Reset
function filterReset () {
	$('.js-filter-color').add('.js-screen-size').removeClass('active');
	$('.js-brand').prop('checked', false);
	$('.newproduct-label__modal').removeClass('show-newproduct-modal');
	$('.ui-editRangeSlider-inputValue').val(0);
	$('.ui-rangeSlider-handle').css('left',0);
	$('.ui-rangeSlider-bar').css('width',0);
}
// End Catalog Filter Reset

// Show Filter Modal
function showFilterModal(self) {
	var modal = $(self).closest('.js-show-parent').find('.js-show-modal');
	$('.js-show-modal').removeClass('show-newproduct-modal');
	modal.removeClass('show-newproduct-modal', function () {
		setTimeout(function () {
			modal.addClass('show-newproduct-modal');
		}, 500);
	});
}
// End Show Filter Modal

function personalAreaSlider() {
  $('.js-slider-personal-area').slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: true,
      dots: true,
      infinite:true,
      focusOnSelect: false,
      variableWidth: true,
      adaptiveHeight: true,
      swipe:true,
      prevArrow: '<div class="prev-not-found"></div>',
      nextArrow: '<div class="next-not-found"></div>',
      responsive: [
          {
              breakpoint: 991,
              settings: {
                  slidesToShow: 2,
              }
          },
          {
              breakpoint: 576,
              settings: {
                  slidesToShow: 1,
              }
          }
      ]
  });
}
