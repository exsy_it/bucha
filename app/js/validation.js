(function() {
	'use strict';

// placeholder
	(function(){
		var input = document.querySelectorAll('.input-group__item');
		var mask = '+7 (___) ___-__-__';
		var maskCard = '____ ____ ____ ____';
		var maskOrder = '____________';
		var maskCardData = '__';
		var maskCardCvv = '___';

		for( var i = 0; input.length > i; i++ ){
			input[i].addEventListener('focus', function () {
				this.nextElementSibling.classList.add('is-active');
			});
			input[i].addEventListener('blur', function () {
				if ( this.value == mask ||
					this.value == maskCard ||
					this.value == maskCardData ||
					this.value == maskOrder ||
					this.value == maskCardCvv)
				{
					this.nextElementSibling.classList.remove('is-active');
				}
				if ( this.value ) return;
				this.nextElementSibling.classList.remove('is-active');
			});
		}
	}());


	$(window).on("load", function() {
// MASK PHONE
		$(function($){
			$(".js-maskPhone").mask("+7 (999) 999-99-99");

			$(".js-cardNumber").mask("9999 9999 9999 9999");
			$(".js-orderNumber").mask("999999999999");
			$(".js-cardMonth").mask("99");
			$(".js-cardYear").mask("99");

			if( $(".js-cardCvv").length > 0 ){
				$(".js-cardCvv").mask("999");
			}
		});

// FORM № ...
		$(function () {
			$("#_code").validate({
				rules: {
					_code: {
						required: true,
						number: true
					}
				},
				messages: {
					_code: {
						required: "",
						number: ""
					}
				},
				invalidHandler: function () {
					$(this).closest('.input-group').addClass('input-group--error');
					//$(this).addClass('block_input__Item--succes');
				},
				submitHandler: function () {
					//$(this).parent('.input-group').removeClass('input-group--error');
				},
				errorClass: 'input-group--error',
				validClass: 'input-group'
			});
			$("#form2").validate({
				rules: {
					username: {
						required: true,
					},
					lastname: {
						required: true,
					}
				},
				messages: {
					username: {
						required: "Проверьте правильность введенных данных!"
					},
					lastname: {
						required: "Проверьте правильность введенных данных!"
					}
				},
				errorClass: 'block_input__Item--error',
				validClass: 'block_input__Item--succes'
			});

		});

	});

}());
